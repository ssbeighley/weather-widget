/*global $, jQuery, alert*/

$(document).ready(function () {
  'use strict';

    $.simpleWeather({
      location: 'Hackettstown, NJ, USA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>Current Conditions for<br />' + weather.city + ', ' + weather.region + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul><small>Updated:' + weather.updated + '';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
  });