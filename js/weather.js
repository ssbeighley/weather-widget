/*! US REFS Security SharePoint
* Weather Widget - v1.9 - 2016-08-10
* Novartis Creative Media & Print */

/*global $, jQuery, alert*/

$(document).ready(function () {
  'use strict';

  if (window.location.href.indexOf("Broomfield") > -1) {
    
    $.simpleWeather({
      location: 'Broomfield, CO, USA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
    
  } else if (window.location.href.indexOf("Cambridge") > -1) {
    
    $.simpleWeather({
      location: 'Cambridge, MA, USA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
    
  } else if (window.location.href.indexOf("Carlsbad") > -1) {
    $.simpleWeather({
      location: 'Carlsbad, CA, USA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
  } else if (window.location.href.indexOf("EastHanover") > -1) {
    $.simpleWeather({
      location: 'East Hanover, NJ, USA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
  } else if (window.location.href.indexOf("Elkridge") > -1) {
    $.simpleWeather({
      location: 'Elkridge, MD, USA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
  } else if (window.location.href.indexOf("Emeryville") > -1) {
    $.simpleWeather({
      location: 'Emeryville, CA, USA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
  } else if (window.location.href.indexOf("FortWorth") > -1) {
    $.simpleWeather({
      location: 'Fort Worth, TX, USA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
  } else if (window.location.href.indexOf("Hicksville") > -1) {
    $.simpleWeather({
      location: 'Hicksville, NY, USA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
  } else if (window.location.href.indexOf("Houston") > -1) {
    $.simpleWeather({
      location: 'Houston, TX, USA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
  } else if (window.location.href.indexOf("Huntington") > -1) {
    $.simpleWeather({
      location: 'Huntington, WV, USA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
  } else if (window.location.href.indexOf("Irvine") > -1) {
    $.simpleWeather({
      location: 'Irvine, CA, USA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
  } else if (window.location.href.indexOf("JohnsCreek") > -1) {
    $.simpleWeather({
      zipcode: '',
      woeid: '55864893', //2357536
      location: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
  } else if (window.location.href.indexOf("LaJolla") > -1) {
    $.simpleWeather({
      location: '',
      woeid: '2434241',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
  } else if (window.location.href.indexOf("LakeForest") > -1) {
    $.simpleWeather({
      location: 'Lake Forest, CA, USA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
  } else if (window.location.href.indexOf("Lesage") > -1) {
    $.simpleWeather({
      location: 'Lesage, WV, USA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather-lesage").html(html);
      },
      error: function (error) {
        $("#weather-lesage").html('<p>' + error + '</p>');
      }
    });
  } else if (window.location.href.indexOf("Melville") > -1) {
    $.simpleWeather({
      location: 'Melville, NY, USA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
  } else if (window.location.href.indexOf("MorrisPlains") > -1) {
    $.simpleWeather({
      location: 'Morris Plains, NJ, USA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
  } else if (window.location.href.indexOf("Princeton") > -1) {
    $.simpleWeather({
      location: 'Princeton, NJ, USA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
  } else if (window.location.href.indexOf("SanCarlos") > -1) {
    $.simpleWeather({
      location: 'San Carlos, CA, USA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
  } else if (window.location.href.indexOf("Wilson") > -1) {
    $.simpleWeather({
      location: 'Wilson, NC, USA',
      woeid: '',
      unit: 'f',
      success: function (weather) {
        var html = '<h3>' + weather.title + '</h3><h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2><ul><li class="currently">' + weather.high + '&deg; / ' + weather.low + '&deg;</li><li class="currently">' + weather.currently + '</li><li>Humidity ' + weather.humidity + '%</li></ul>';

        $("#weather").html(html);
      },
      error: function (error) {
        $("#weather").html('<p>' + error + '</p>');
      }
    });
  }

});